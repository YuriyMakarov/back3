<?php
// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // В суперглобальном массиве $_GET PHP хранит все параметры, переданные в текущем запросе через URL.
    if (!empty($_GET['save'])) {
        // Если есть параметр save, то выводим сообщение пользователю.
        print('Спасибо, результаты сохранены.');
    }
    // Включаем содержимое файла form.php.
    include('form.php');
    // Завершаем работу скрипта.
    exit();
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.

// Проверяем ошибки.
$errors = FALSE;
if (empty($_POST['name1'])) {
    print('Заполните имя.<br/>');
    $errors = TRUE;
}

// *************
// Тут необходимо проверить правильность заполнения всех остальных полей.
if (empty($_POST['Year1'])) {
    print('Заполните год рождения.<br/>');
    $errors = TRUE;
}
if (!isset($_POST['Sex1'])) {
    print('Выберите пол.<br/>');
    $errors = TRUE;
}
if (!isset($_POST['limb1'])) {
    print('Выберите к-во конечностей.<br/>');
    $errors = TRUE;
}
if (!isset($_POST['Check1'])) {
    print('Подтверите согласие.<br/>');
    $errors = TRUE;
}

// *************

if ($errors) {
    // При наличии ошибок завершаем работу скрипта.
    exit();
}

// Сохранение в базу данных.

$user = 'u21044';
$pass = '76898768';

try {
$db = new PDO('mysql:host=localhost;dbname=u21044', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
} catch (PDOException $e) {
        // Если есть ошибка соединения или выполнения запроса, выводим её
        print "Ошибка!: " . $e->getMessage() . "<br/>";
    }

$Fio = $_POST['name1'];
$Mail = $_POST['email1'];
$Data = $_POST['Year1'];
$Gend = $_POST['Sex1'];
$Limb = $_POST['limb1'];
$Ability = $_POST['Power1'];
$Biog = $_POST['Biography1'];

	


//  Именованные метки.
$stmt = $db->prepare("INSERT INTO application (fio, email, birthdate, sex, limbs, biography) VALUES (:name, :email, :year, :gender, :CountLimbs, :bio)");
$stmt -> execute(array('name'=>$Fio, 'email'=>$Mail,'year'=>$Data,'gender'=>$Gend,'CountLimbs'=>$Limb, 'bio'=>$Biog));
$stmt = $db->prepare('insert into app_sup set id = ?, sup_id = ?');
$id = $db->lastInsertId();
foreach ($Ability as $val){
    $stmt->execute([$id, $val]);
}
header('Location: ?save=1');
